process.env.BROWSER = "none";
const path = require('path');

const CracoAntDesignPlugin = require("craco-antd");

module.exports = {
    plugins: [
        // { plugin: require("craco-preact") },
        {
          plugin: CracoAntDesignPlugin,
          options: {
            customizeThemeLessPath: path.join(
              __dirname,
              "src/theme/customTheme.less"
            ),
          },
        },
    ],
  };

export {}   