import React, {useState, useEffect} from 'react'
import Axios from "axios";

const useApiRequest = (url:string) => {
    const [data, setData] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);
  
    const getData = async () => {
       await Axios
        .get(url)
        .then(response => {
          setIsLoaded(true);
          setData(response.data);
        })
        .catch(error => {
          setError(error);
        });
      };
      useEffect(() => {
        getData();
      }, []);
      
    return { error, isLoaded, data };
  };



  export default useApiRequest;