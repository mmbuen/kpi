
import { useEffect, useState } from "react"
import  Axios  from "axios";

const MultiApiRequest = (urls:Object) => {
    const [results, setResults] = useState({});
    const [prop, setProp] = useState("");
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null); 

    const getData = async () => {
      await Promise.all(Object.entries(urls).map(([propertyName, url]) => (
        Axios
        .get(url)
      ))
      ).then(response => {
        setResults({dept:response[0].data, kpiDetails:response[1].data})
      })
      .catch(error => {
        setError(error);
      })

      // const apiUrls = Object.entries(urls).map(([propertyName,url])=>(url))
      // Promise.all(Object.entries(urls).map(([propertyName,url])=> 
      //     fetch(url)
      //     .then(res=>res.json())
      //   )).then(data=>{
      //     setResults({dept:data[0], kpiDetails:data[1]})
      //   })
    };
       
    useEffect(() => {
      getData().then(()=>{setIsLoaded(true)});
      
       }, []);
    
       return { error, isLoaded, results };
};

export default MultiApiRequest;