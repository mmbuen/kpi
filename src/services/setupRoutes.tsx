import React from 'react'
import { NavLink, Routes, Route, Link, useNavigate, useLocation} from 'react-router-dom';
import Dashboard from '../views/Dashboard';
import KPI from '../views/KPI';


const Routing = () => {
  const routingEnum = [{path:'/dashboard', element:<Dashboard/>}, {path:'/kpi', element:<KPI/>}]
  const routing  = routingEnum.map((routes)=> 
  <Route path={routes.path} element={routes.element}></Route>
  );
  
  return(
            <Routes>
                {routing}
            </Routes>
        )
}



export default Routing;