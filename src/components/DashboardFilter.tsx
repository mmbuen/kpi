import { PageContainer } from '@ant-design/pro-layout'
import { DatePicker, Descriptions, Tree, TreeSelect, Typography } from 'antd'
import { DataNode } from 'antd/lib/tree';
import moment from 'moment';

import React, { useState, useEffect } from 'react'

const {Title, Paragraph} = Typography;
const { TreeNode } = TreeSelect;
const {RangePicker} = DatePicker;

const DashboardFilter = ({data}:any) => {
    const [treeLine, setTreeLine] = useState(true);
    const [showLeafIcon, setShowLeafIcon] = useState(false);
  
    const deptTree = data?.map((agm:any)=>({title:agm.AGMGroup, key:agm.AGMCode, value:agm.AGMGroup, children:agm.Departments?.map((dept:any)=>({title:dept.Acronym, key:dept.DepartmentCode, value:dept.Acronym, isLeaf:true}))}));
    //console.log(deptTree)
    return (
    <div className="site-page-header-ghost-wrapper">
  <PageContainer
  subTitle="-- 2022"
    content={
    <Descriptions
      // title="Responsive Descriptions"
      bordered
      column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        
        <Descriptions.Item label="Description" span={4}>
          <Paragraph>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia corporis nobis distinctio perspiciatis ullam quisquam suscipit quo. Id harum ad blanditiis, deserunt fugit, vero nesciunt veniam exercitationem porro rerum pariatur?
          </Paragraph>
        </Descriptions.Item>

        <Descriptions.Item label="Month">
          <RangePicker picker="month" 
          format="yyyyMM"
          ranges={{
            'This Month': [moment().startOf('month'), moment().endOf('month')],
          }
          }
        disabledDate={current => {
          return current > moment().endOf('month');
          }}
          />
        </Descriptions.Item>

        <Descriptions.Item label="Department">
          
          <TreeSelect treeLine={treeLine && { showLeafIcon } } style={{ width: 300 }} placeholder="Select department" showSearch treeData={deptTree}/>

        </Descriptions.Item>
    </Descriptions>
  }>
    </PageContainer>
  </div>
  )
}

export default DashboardFilter