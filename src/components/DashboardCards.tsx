import { EyeOutlined, EllipsisOutlined } from '@ant-design/icons';
import { ModalForm, ProCard, StatisticCard } from '@ant-design/pro-components';
import { Row, Col, Card, message,  Typography, TreeSelect, DatePicker } from 'antd';
import React, { useState } from 'react'
import RcResizeObserver from 'rc-resize-observer';


const { Statistic } = StatisticCard;

const DashboardCards = ({data}:any) => {

  const [responsive, setResponsive] = useState(false);

    const waitTime = (time: number = 100) => {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve(true);
          }, time);
        });
      };

  return (
    <Row 
        gutter={[16,{
        xs: 8,
        sm: 16,
        md: 24,
        lg: 32,
      }]}
        style={{padding:50}}  
      >{data?.map((entry: any)=>(
      <Col xs={24} md={12} lg={6}>
          
              <Card title={entry.PerfInd} bordered={false} loading={false}
              actions={[
                <ModalForm
                  title="Percentage of ..."
                  trigger={<EyeOutlined key="view" />}
                  width={1500}
                  submitter={{
                    searchConfig: {
                      submitText: '确认',
                      resetText: '取消',
                    },
                  }}
                  onFinish={async (values) => {
                    await waitTime(2000);
                    console.log(values);
                    message.success('提交成功');
                    return true;
                  }}
                >
                  <RcResizeObserver
                    key="resize-observer"
                    onResize={(offset) => {
                      setResponsive(offset.width < 596);
                    }}
                  >
                    <ProCard split={responsive ? 'horizontal' : 'vertical'}>
                      <StatisticCard
                        colSpan={responsive ? 24 : 6}
                        title="March 2022"
                        //headStyle={{backgroundColor:"rgb(12,81,139)", color:"rgb(255,255,255)"}}
                        statistic={{
                          value: 64.7,
                          suffix: '%',
                          description: <Statistic title="March 2022" value="0.47%" trend="up" />,
                        }}
                        chart={
                          <img
                            src="https://gw.alipayobjects.com/zos/alicdn/PmKfn4qvD/mubiaowancheng-lan.svg"
                            alt="进度条"
                            width="100%"
                          />
                        }
                        footer={
                          <>
                            <Statistic value="64.52%" title="Year to Date" layout="horizontal" />
                            <Statistic value="65%" title="Success (1st sem)" layout="horizontal" />
                            <Statistic value="67%" title="Success (2nd sem)" layout="horizontal" />
                          </>
                        }
                      />
                      <StatisticCard.Group
                        colSpan={responsive ? 24 : 18}
                        direction={responsive ? 'column' : undefined}
                      >
                        <StatisticCard
                          statistic={{
                            title: 'Number of residential (household) connections with lone metering',
                            value: 1072,
                            description: <Statistic title="wow" value="6.15%" trend="up" />,
                          }}
                          chart={
                            <img
                              src="https://gw.alipayobjects.com/zos/alicdn/zevpN7Nv_/xiaozhexiantu.svg"
                              alt="sample"
                              width="100%"
                            />
                          }
                        >
                          <Statistic
                            title="大盘总收入"
                            value={1982312}
                            layout="vertical"
                            description={<Statistic title="日同比" value="6.15%" trend="down" />}
                          />
                        </StatisticCard>
                        <StatisticCard
                          statistic={{
                            title: '当日排名',
                            value: 6,
                            description: <Statistic title="日同比" value="3.85%" trend="down" />,
                          }}
                          chart={
                            <img
                              src="https://gw.alipayobjects.com/zos/alicdn/zevpN7Nv_/xiaozhexiantu.svg"
                              alt="折线图"
                              width="100%"
                            />
                          }
                        >
                          <Statistic
                            title="近7日收入"
                            value={17458}
                            layout="vertical"
                            description={<Statistic title="日同比" value="6.47%" trend="up" />}
                          />
                        </StatisticCard>
                        <StatisticCard
                          statistic={{
                            title: '财年业绩收入排名',
                            value: 2,
                            description: <Statistic title="日同比" value="6.47%" trend="up" />,
                          }}
                          chart={
                            <img
                              src="https://gw.alipayobjects.com/zos/alicdn/zevpN7Nv_/xiaozhexiantu.svg"
                              alt="折线图"
                              width="100%"
                            />
                          }
                        >
                          <Statistic
                            title="月付费个数"
                            value={601}
                            layout="vertical"
                            description={<Statistic title="日同比" value="6.47%" trend="down" />}
                          />
                        </StatisticCard>
                      </StatisticCard.Group>
                    </ProCard>
                    </RcResizeObserver>
                </ModalForm>,
    
                <EllipsisOutlined key="ellipsis" />,
              ]}
              headStyle={{backgroundColor:"rgb(12,81,139)", color:"rgb(255,255,255)", fontSize:"10px"}}
              >
                <Statistic
                  value={entry.Figure}
                  precision={2}
                  valueStyle={{ color: '#3f8600', fontSize:'25px' }}
                  suffix="%" 
                />  
              </Card>

         
          
      </Col>    ))} 
    </Row>
  )
}

export default DashboardCards