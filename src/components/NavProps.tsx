import { AntDesignOutlined, CrownOutlined, SmileOutlined, AppstoreOutlined,
    UnorderedListOutlined } from '@ant-design/icons';
import React from 'react';
import Dashboard from '../views/Dashboard';

export default {
  route: {
    // path: '/',
    routes: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        desc: 'dasbordz',
        icon: <AppstoreOutlined />,
        component: './Dashboard',
      },
      {
        path: '/kpi',
        name: 'List of KPIs',
        desc: 'keypieye',
        icon: <UnorderedListOutlined />,
        component: './KPI',
      }
      
    ],
  },
};