import React, {useState, useEffect} from 'react'
import ProLayout, { MenuDataItem, PageContainer } from '@ant-design/pro-layout';
import { NavLink, Routes, Route, Link, useNavigate, useLocation} from 'react-router-dom';
import NavProps from './NavProps';

import Dashboard from '../views/Dashboard'
import KPI from '../views/KPI';

import Routing from '../services/setupRoutes'
import dcwdLogo from '../assets/img/dcwd-logo.png'

// const publicPath = '/dcwdApps/kpidashboard/';

// export const routeCodes = {
//   HOME: publicPath,
//   DASHBOARD: `${ publicPath }dashboard`,
//   KPI: `${ publicPath }KPI`,
// };
// console.log(process.env.PUBLIC_URL);

const NavProLayout = () => {
    
const loc = useLocation().pathname
const [pathname, setPathname] = useState(loc);
const navigate = useNavigate();


  return (
    
    <div
      style={{
        height: '100vh',
      }}
    >
      <ProLayout
      {...NavProps}
      title={"CPD-KPI"}
      logo={dcwdLogo}
      location={{
        pathname,
      }}
      navTheme={"light"}
      fixSiderbar={true}
      iconfontUrl="//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js"
      onMenuHeaderClick={() => {setPathname('/dashboard'); navigate('/dashboard')}}
      menuItemRender={(item:any, dom:React.ReactNode) => (
      //   <a
      //   onClick={() => {
      //     setPathname(item.path || '/');
      //   }}
      //   >
      //   {dom}
      // </a>
      item.isUrl ? (
        dom
      ) :

      (
        <Link to={item.path || '/'} onClick={() => {
              setPathname(item.path || '/');
            }}>
          {dom}
        </Link>
      )

      )}
    >
    
    <Routing/>
     
    </ProLayout>
    </div>
  )
}

export default NavProLayout