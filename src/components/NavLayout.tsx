import React from 'react'
import { Layout, Menu, BackTop, Skeleton, Image} from 'antd';
import { NavLink, Routes, Route, Link} from 'react-router-dom';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  AppstoreOutlined,
  UnorderedListOutlined,
  UpOutlined

} from '@ant-design/icons';
import Dashboard from '../views/Dashboard'
import KPI from '../views/KPI';

import './NavLayout.css'

const { Header, Sider, Content, Footer } = Layout;

const items = [
    {
      key: '1',
      icon: <AppstoreOutlined />,
      label: 'Dashboard',
      path: '/dashboard',
    },
    {
      key: '2',
      icon: <UnorderedListOutlined />,
      label: 'List of KPIs',
      path:'/kpi'
    },
]

const backTopStyle: React.CSSProperties = {
  lineHeight: '40px',
  borderRadius: 50,
  backgroundColor: '#1088e9',
  color: '#fff',
  textAlign: 'center',
  fontSize: 14,
};

const NavLayout:React.FC = () => {
  const [collapsed, setCollapsed] = React.useState(false);

const toggle = () => {
    setCollapsed(!collapsed);
    console.log(collapsed);
};
{/* <Skeleton/> */}

  return (
    <Layout style={{minHeight:"200vh"}}>
      <Sider
      collapsible
      collapsed={collapsed}
      trigger={null}
      theme="light"
      width={270}
      >
        <div className="logo">
        <img
          src="img/dcwd-logo.png"
          style={{
            width: 36,
            height: 36,
            margin: '0 6px',
            marginRight: 10,
          }}
        />
          KPI
        </div>
        <NavLink to="/dashboard">
          <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={['1']}
          //items={items}
        >
          {items.map((item) => (
            <Menu.Item icon={item.icon} key={item.key}>{item.label}<Link to={item.path} ></Link></Menu.Item>
          ))}
        </Menu>
        </NavLink>
      </Sider>
      {/* <Sidebar collapsed={collapsed}/> */}
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => toggle(),
          })}
        </Header>
        <Content
          //className="site-layout-background"
          style={{
            //margin: '24px 16px',
            // padding: 50,
            
          }}
        >
          <Routes>
            <Route path='/dashboard' element={<Dashboard/>}></Route>
            <Route path='/kpi' element={<KPI/>}></Route>
          </Routes> 
        </Content>

        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
      <BackTop>
      <div style={backTopStyle}><UpOutlined /></div>
      </BackTop>
    </Layout>
  )
}

export default NavLayout



// class SiderDemo extends React.Component {
//   state = {
//     collapsed: false,
//   };
//   toggle = () => {
//     this.setState({
//       collapsed: !this.state.collapsed,
//     });
//   }
// }