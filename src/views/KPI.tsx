import React, {useState, useEffect} from 'react'
import { PageHeader, Descriptions, TreeSelect, Button, Table} from 'antd';
import { PageContainer } from '@ant-design/pro-layout';

import { PlusOutlined } from '@ant-design/icons';
import { ProColumns, ProList } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';

import type { ColumnsType, TableProps } from 'antd/lib/table';

import Axios from "axios";

import useApiRequest from "../services/fetchData";


interface KPIItem  {
    Period:string
		 CritName:string
		 CritSort:string
		 Mfo:string
		 PerfInd:string
		 Figure:string
		 CompName:string

		 Dept:string
		 AGMGroup:string
		 FigureComp:string
		 FigureYTD:string
};

const columnsTable: ColumnsType<KPIItem> = [
  {
    title: 'Period',
    dataIndex: 'Period',
    key: 'period',
  },
  { 
    title: 'CritName',
    dataIndex: 'CritName',
    key: 'critname',
  },
  {
    title: 'CritSort',
    dataIndex: 'CritSort',
    key: 'critsort',
  },
  {
    title: 'Mfo',
    dataIndex: 'Mfo',
    key: 'mfo',
  },
  {
    title: 'PerfInd',
    dataIndex: 'PerfInd',
    key: 'perfind',
  },
  {
    title: 'Figure',
    dataIndex: 'Figure',
    key: 'figure',
  },
  {
    title: 'CompName',
    dataIndex: 'CompName',
    key: 'compname',
  },
  {
    title: 'Dept',
    dataIndex: 'Dept',
    key: 'dept',
  },
  {
    title: 'AGMGroup',
    dataIndex: 'AGMGroup',
    key: 'agmgroup',
  },
  {
    title: 'FigureComp',
    dataIndex: 'FigureComp',
    key: 'figurecomp',
  },
  {
    title: 'FigureYTD',
    dataIndex: 'FigureYTD',
    key: 'figureytd',
  },

];

const columns: ProColumns<KPIItem>[] = [
  {
    title: 'Period',
    dataIndex: 'Period',
    key: 'period',
  },
  { 
    title: 'CritName',
    dataIndex: 'CritName',
    key: 'critname',
  },
  {
    title: 'CritSort',
    dataIndex: 'CritSort',
    key: 'critsort',
  },
  {
    title: 'Mfo',
    dataIndex: 'Mfo',
    key: 'mfo',
  },
  {
    title: 'PerfInd',
    dataIndex: 'PerfInd',
    key: 'perfind',
  },
  {
    title: 'Figure',
    dataIndex: 'Figure',
    key: 'figure',
  },
  {
    title: 'CompName',
    dataIndex: 'CompName',
    key: 'compname',
  },
  {
    title: 'Dept',
    dataIndex: 'Dept',
    key: 'dept',
  },
  {
    title: 'AGMGroup',
    dataIndex: 'AGMGroup',
    key: 'agmgroup',
  },
  {
    title: 'FigureComp',
    dataIndex: 'FigureComp',
    key: 'figurecomp',
  },
  {
    title: 'FigureYTD',
    dataIndex: 'FigureYTD',
    key: 'figureytd',
  },

];


const KPI = () => {
  // const [post, getPost] = useState([])
  const API = '/api/KPI/details';
  const { data, error, isLoaded } = useApiRequest(API);

  // const [state, setstate] = useState([]);
  // const [loading, setloading] = useState(true);


  // const getData = async () => {
  //   await Axios.get(API).then(
  //     res => {
  //       setloading(false);
  //       setstate(
  //         res.data.map((row:KPIItem) => ({
  //           Period:row.Period,
  //           CritName:row.CritName,
  //           CritSort:row.CritSort,
  //           Mfo:row.Mfo,
  //           PerfInd:row.PerfInd,
  //           Figure:row.Figure,
  //           CompName:row.CompName,
  //           Dept:row.Dept,
  //           AGMGroup:row.AGMGroup,
  //           FigureComp:row.FigureComp,
  //           FigureYTD:row.FigureYTD,
  //         }))
  //       );
  //     }
  //   );
  // };
  // useEffect(() => {
  //   getData();
  // }, []);

  // const data: KPIItem[] = post

  return (
    <div className="site-page-header-ghost-wrapper">
    {/* <PageHeader
      ghost={false}

      
      // title="List of KPIs"
      // subTitle="This is a subtitle"

    >
      <Descriptions
      // title="Responsive Descriptions"
      bordered
      column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        <Descriptions.Item label="Month">
          Sample
        </Descriptions.Item>
    </Descriptions>

    </PageHeader> */}

    <PageContainer fixedHeader>

    <Table columns={columnsTable} dataSource={data} loading={!isLoaded}/>

    <ProTable<KPIItem>
        columns={columns}
        request={async () => {
          return Promise.resolve({
            data: data,
            success: true,
          });
        }}
        loading={!isLoaded}
        rowKey="key"
        dateFormatter="string"
        headerTitle="List of KPIs"
        search={{
          defaultCollapsed: false,
          labelWidth: 'auto',
          optionRender: (searchConfig, formProps, dom) => [
            
            ...dom.reverse(),
            <Button key="out" onClick={() => {}}>
              Search
            </Button>
          ],
        }}
        toolBarRender={() => [
          <Button key="primary" type="primary">
            <PlusOutlined />
            Sample add
          </Button>,
        ]}
    />

</PageContainer>
    </div>
        
    

  )
}

export default KPI