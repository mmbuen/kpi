import React, { useState } from 'react';

import useApiRequest from '../services/fetchData';
import DashboardCards from '../components/DashboardCards';
import DashboardFilter from '../components/DashboardFilter';
import MultiApiRequest from '../services/fetchMultiple';
import { Skeleton, Spin } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';


const style = {
  background: '#0092ff',
  padding: '8px 0',
};



const Dashboard:React.FC  = () => {
  // const [treeLine, setTreeLine] = React.useState(true);
  // const [showLeafIcon, setShowLeafIcon] = useState(false);
  // const [responsive, setResponsive] = useState(false);

  // const waitTime = (time: number = 100) => {
  //   return new Promise((resolve) => {
  //     setTimeout(() => {
  //       resolve(true);
  //     }, time);
  //   });
  // };

  const API = '/api/Department';
  const dataAPI = '/api/KPI/details';

  const urls = [
    '/api/Department',
    '/api/KPI/details'
  ]

  const apiUrls = {
    dept: '/api/Department',
    kpiDetails: '/api/KPI/details'
  }

  const testData = [{mfoName:"Percentage of ...", mfoFigure:"65"}, {mfoName:"Number of ...", mfoFigure:"555"}]
  const testDept = [{AGMGroup:"AGMA", Departments:[{Acronym:"GSD"}]}]
  // console.log(testData);


  // const allData = multiApiRequest(urls);
  
  //  const { data, error, isLoaded } = useApiRequest(dataAPI);
  const {results, error, isLoaded} = MultiApiRequest(apiUrls);

  console.log("results: ",results)
  console.log("object: ",Object.entries(results))
  console.log("object keys", Object.keys(results))
  console.log("object values", Object.values(results))

  return(
  
  <>


  
    {!isLoaded ? <DashboardFilter><Skeleton active/></DashboardFilter> : <DashboardFilter data={Object.values(results)[0]}/>}

    {!isLoaded ? <DashboardCards><Skeleton active/></DashboardCards> : <DashboardCards data={Object.values(results)[1]}/>}
    {/* {!isLoaded ? <Skeleton active/> : <DashboardCards data={data}/>} */}


      
  </>
  );
};
export default Dashboard;